require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

Vue.use(VueAxios, axios);

import CKEditor from '@ckeditor/ckeditor5-vue';

Vue.use( CKEditor );

window.axios = axios;

axios.defaults.headers.common = {
    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
    'X-Requested-With': 'XMLHttpRequest'
};

// toaster
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/index.css';
Vue.use(VueToast);

import MainComponent from './components/MainComponent.vue';

import DashboardComponent from './components/DashboardComponent.vue';
import UsersComponent from './components/users/UsersComponent.vue';
import AddUserComponent from './components/users/AddUserComponent.vue';
import EditUserComponent from './components/users/EditUserComponent.vue';
import ChangeUserPasswordComponent from './components/users/ChangeUserPasswordComponent.vue';
import ReportsComponent from './components/reports/ReportsComponent.vue';
import SettingComponent from './components/SettingComponent.vue';
import PackageComponent from './components/packages/PackageComponent.vue';
import AddPackageComponent from './components/packages/AddPackageComponent.vue';
import EditPackageComponent from './components/packages/EditPackageComponent.vue';
import TestimonialsComponent from './components/testimonials/TestimonialsComponent.vue';
import AddTestimonialComponent from './components/testimonials/AddTestimonialComponent.vue';
import EditTestimonialComponent from './components/testimonials/EditTestimonialComponent.vue';
import BlogComponent from './components/blogs/BlogComponent.vue';
import AddBlogsComponent from './components/blogs/AddBlogsComponent.vue';
import EditBlogsComponent from './components/blogs/EditBlogsComponent.vue';
import AccountProfileComponent from './components/users/AccountProfileComponent.vue';


const routes = [

    {
      name: 'admin',
      path: '/admin',
      component: DashboardComponent
    },
  	{
      name: 'users',
      path: '/admin/users',
      component: UsersComponent
  	},
    {
      name: 'adduser',
      path: '/admin/add-user',
      component: AddUserComponent
    },
    {
      name: 'updateuser',
      path: '/admin/update-user/:id',
      component: EditUserComponent
    },,
    {
      name: 'changepassword',
      path: '/admin/change-password/:id',
      component: ChangeUserPasswordComponent
    },
    {
      name: 'settings',
      path: '/admin/settings',
      component: SettingComponent
    },
    {
      name: 'packages',
      path: '/admin/packages',
      component: PackageComponent
    },
    {
      name: 'addpackage',
      path: '/admin/add-package',
      component: AddPackageComponent
    },
    {
      name: 'updatepackage',
      path: '/admin/update-package/:id',
      component: EditPackageComponent
    },
    {
      name: 'testimonials',
      path: '/admin/testimonials',
      component: TestimonialsComponent
    },
    {
      name: 'addtestimonial',
      path: '/admin/add-testimonial',
      component: AddTestimonialComponent
    },
    {
      name: 'updatetestimonial',
      path: '/admin/update-testimonial/:id',
      component: EditTestimonialComponent
    },
    {
      name: 'blogs',
      path: '/admin/blogs',
      component: BlogComponent
    },
    {
      name: 'addblog',
      path: '/admin/add-blog',
      component: AddBlogsComponent
    },
    {
      name: 'updateblog',
      path: '/admin/update-blog/:id',
      component: EditBlogsComponent
    },
    {
      name: 'updateprofile',
      path: '/admin/update-profile',
      component: AccountProfileComponent
    }
];

const router = new VueRouter({ mode: 'history', routes: routes});
const app = new Vue(Vue.util.extend({ router }, MainComponent)).$mount('#app');