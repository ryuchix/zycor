	<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/main.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/lib/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/app-form-elements.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/lib/summernote/summernote.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/lib/summernote/summernote-ext-beagle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			//initialize the javascript
			App.init();
			App.formElements();
			App.textEditors();
		});
	</script>