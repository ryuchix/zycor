<div class="be-left-sidebar">
	<div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Blank Page</a>
		<div class="left-sidebar-spacer">
			<div class="left-sidebar-scroll">
				<div class="left-sidebar-content">
					<ul class="sidebar-elements">
						<li class="divider">Menu</li>
						<li><a href="index.html"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a>
						</li>
						<li class="parent"><a href="#"><i class="icon mdi mdi-face"></i><span>Users</span></a>
							<ul class="sub-menu">
								<li><a :href="users">Users</a>
								</li>
								<li><a href="ui-buttons.html">Add User</a>
								</li>
							</ul>
						</li>
						<li class="parent"><a href="#"><i class="icon mdi mdi-alert-circle"></i><span>Reports</span></a>
							<ul class="sub-menu">
								<li><a href="form-elements.html">Reports</a>
								</li>
							</ul>
						</li>
						<li class="divider">Settings</li>
						<li><a href="charts.html"><i class="icon mdi mdi-settings"></i><span>Settings</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>