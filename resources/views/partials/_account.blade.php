<ul class="nav navbar-nav navbar-right be-user-nav">
	<li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="assets/img/avatar.png" alt="Avatar"><span class="user-name">Túpac Amaru</span></a>
		<ul role="menu" class="dropdown-menu">
			<li>
				<div class="user-info">
					<div class="user-name">Ryok</div>
					<div class="user-position online">Available</div>
				</div>
			</li>
			<li><a href="#"><span class="icon mdi mdi-face"></span> Account</a></li>
			<li><a href="#"><span class="icon mdi mdi-settings"></span> Settings</a></li>
			<li><a href="#"><span class="icon mdi mdi-power"></span> Logout</a></li>
		</ul>
	</li>
</ul>