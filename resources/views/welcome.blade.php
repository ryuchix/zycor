@include('partials._header')
<body>
<div class="be-wrapper">
  <div id="app">
    <MainComponent></MainComponent>
  </div>
</div>
@include('partials._script')
@include('partials._footer')