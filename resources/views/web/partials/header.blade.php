<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<title>Zycor Fly Travel and Tours | Cheap and Affordable Airfare and Tour Packages</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="weibergmedia">
	<meta name="Description" content="Zycor Fly Travel and Tours is a davao based travel agency that provides cheap flight and affordable tour packages for domestic and international. Also helps travelers for their visa processess and application." />

	<meta name="author" content="Irene Corpuz">
	<meta name="keywords" content="Cheap travel and tours, zycor fly, cheap airfare, peso fare, cebu pacific peso fair, philippine airline peso fare, davao city travel agency, visa assistance, visa approval">
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Zycor Fly Travel and Tours | Cheap and Affordable Airfare and Tour Packages" />
	<meta property="og:description" content="Zycor Fly Travel and Tours | Cheap and Affordable Airfare and Tour Packages is a davao based travel agency that provides cheap flight and affordable tour packages for domestic and international. Also helps travelers for their visa processess and application." />
	<link rel="canonical" href="https://www.zycorfly.com" />
	<meta property="og:url" content="https://www.zycorfly.com" />
	<meta property="og:site_name" content="Zycor Fly Travel and Tours | Cheap and Affordable Airfare and Tour Packages" />
	<meta property="og:image" content="{{ asset('images/zycorhero.png') }}" />
	<meta property="og:image:secure_url" content="{{ asset('images/zycorhero.png') }}" />
	<meta property="og:image:width" content="640" />
	<meta property="og:image:height" content="380" />



	<link href="{{ asset('web/css/reset.css') }}" rel="stylesheet" type="text/css" media="screen" />
	<link href="{{ asset('web/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('web/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('web/css/contact.css') }}" rel="stylesheet" type="text/css" media="screen" />
	<link href="{{ asset('web/css/styles.css') }}" rel="stylesheet" type="text/css" media="screen" />
	<link href="{{ asset('web/css/flexslider.css') }}" rel="stylesheet" type="text/css" media="screen">
	<link href="{{ asset('web/css/jquery.fancybox.css') }}" rel="stylesheet" type="text/css" media="screen" />
	<link href="{{ asset('web/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" media="screen">
	<link href="{{ asset('web/css/responsive.css') }}" rel="stylesheet" type="text/css" media="screen" />
	<link href="http://fonts.googleapis.com/css?family=Oswald:400,600,700" rel="stylesheet" type="text/css" />
	<link href="http://fonts.googleapis.com/css?family=Lora:400,400italic,600" rel="stylesheet" type="text/css" />
	<script src="{{ asset('web/js/modernizr.custom.js') }}" type="text/javascript"></script>
	<link href="{{ asset('web/css/custom.css') }}" rel="stylesheet" type="text/css" media="screen" />

</head>
<body>
	<!-- preloader -->
	<div id="preloader">
		<div id="status">
			<div class="parent">
				<div class="child">
					<p class="small">loading...</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end preloader --> 