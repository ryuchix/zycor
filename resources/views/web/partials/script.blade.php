<script src="{{ asset('web/js/jquery-1.12.4.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('web/js/jquery-easing-1.3.js') }}" type="text/javascript"></script> 
<script src="{{ asset('web/js/jquery.touchSwipe.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('web/js/jquery.isotope2.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('web/js/packery-mode.pkgd.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('web/js/jquery.isotope.load.js') }}" type="text/javascript"></script> 
<script src="{{ asset('web/js/jquery.nav.js') }}" type="text/javascript"></script> 
<script src="{{ asset('web/js/responsive-nav.js') }}" type="text/javascript"></script> 
<script src="{{ asset('web/js/jquery.sticky.js') }}" type="text/javascript"></script> 
<script src="{{ asset('web/js/jquery.form.js') }}" type="text/javascript"></script> 
<script src="{{ asset('web/js/starter.js') }}" type="text/javascript"></script> 
<script src="{{ asset('web/js/jquery.flexslider-min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('web/js/ajax.js') }}"></script> 
<script src="{{ asset('web/js/bootstrap.min.js') }}"></script> 
<script src="{{ asset('web/js/bootstrap-datepicker.min.js') }}"></script> 
<script src="{{ asset('web/js/jquery.fitvids.js') }}" type="text/javascript"></script> 
<script src="{{ asset('web/js/jquery.fancybox.pack.js') }}" type="text/javascript"></script> 
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<script>
$(window).load(function() {
  $('#package-slider').carousel({wrap:true});
});
</script>

@yield('script')

</body>
</html>