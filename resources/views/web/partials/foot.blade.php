<footer>
  <div class="container clearfix">
    <div class="col-lg-12"> <span class="alignleft small">{!! nl2br(e($setting->footer_copyright)) !!}</span> <span class="alignright small">Made with <i class="fa fa-heart"></i> by <a target="_blank" href="https://www.randyph.com" data-title="Professional Web design in Davao">Ryok</a>. </span> </div>
  </div>
</footer>