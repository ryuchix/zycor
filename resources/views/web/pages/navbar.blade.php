<!-- start header -->
<header class="clearfix">
  <div id="logo"> <a style="font-family: Zycor; font-size: 48px;" href="{{ url('/') }}">Zycor Fly</a> </div>
  <div class="tagline"><span>{!! nl2br(e($setting->site_subheading)) !!}</span></div>
  <div id="nav-button"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
  <nav>
    <ul id="nav">
      <li class="active"><a href="#section1">Home</a> </li>
      <li><a href="#packages">Tour Packages</a> </li>
      <li><a href="#visa">Visa Assistance</a> </li>
      <li><a href="#">About</a></li>
      <li><a href="{{ url('blog') }}">Blog</a></li>
      <li><a href="#contact">Contact</a> </li>
    </ul>
  </nav>
</header>
<!-- end header --> 