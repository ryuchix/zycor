<section class="section" id="packages">
	<div id="ancor5"></div>
	<div class="container clearfix">
		<div class="col-lg-12 centered header-wrapper">
			<h2>{!! nl2br(e($setting->tour_title)) !!}</h2>
			<p class="large">{!! nl2br(e($setting->tour_subtitle)) !!}</p>
		</div>

		<div id="package-slider" class="carousel slide">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				@php $count = 0; @endphp
				@foreach($packages->chunk(3) as $chunk)
				<li data-target="#package-slider" data-slide-to="{{ $count }}" class="{{ $count == 0 ? 'active' : '' }}" style="background-color: #e8d0d0; border: 1px solid #f3f3f3;"></li>
				@php $count++; @endphp
				@endforeach
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<!-- Slide -->
				@php $count = 1; @endphp
				@foreach($packages->chunk(3) as $chunk)
				<div class="item {{ $count == 1 ? 'active' : '' }}">
					@foreach($chunk as $package)
					<div class="col-lg-4 col-md-4"> 
						<img alt="" src="{{ asset('images/packages/'.$package->image) }}" />
						<figure class="price-table {{ $package->highlight == 1 ? 'highlighted' : '' }}">
							<div class="heading">
								@if($package->highlight == 1)
								<p class="small">{{ $package->highlight_name }}</p>
								@endif
								<h4>{{ $package->name }}</h4>
							</div>
							<p class="price"><span>₱</span>{{ $package->price }}</p>
							<p class="price-details">
								{!! nl2br(e($package->details)) !!}
							</p>
							<a href="{{ url('package/'.$package->slug) }}" class="button"><i class="fa fa-shopping-cart"></i> See details</a>
				
						</figure>
					</div>
					@endforeach 
				</div>
				@php $count++; @endphp
				@endforeach
			</div>

			<!-- Controls -->
			<a class="left carousel-control" style="color: #000; " href="#package-slider" data-slide="prev">
				<span class="icon-prev" style="font-size: 70px"></span>
			</a>
			<a class="right carousel-control" style="color: #000; " href="#package-slider" data-slide="next">
				<span class="icon-next" style="font-size: 70px"></span>
			</a>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="packagemodal" tabindex="-1" role="dialog" aria-labelledby="packagemodal" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="titlehere"></h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="right: 15px; top: 15px; position: absolute;">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<div id="contenthere"></div>
	      </div>
	      <div class="modal-footer">
	        <button class="button" data-toggle="modal" data-target="#packagemodal"><i class="fa fa-close"></i> Close</button>
	      </div>
	    </div>
	  </div>
	</div>
</section>

@section('script')
<script>
	$('.openmodal').on('click', function(){
		let content = $(this).data('content');
		let name = $(this).data('title');
		$('#titlehere').text(name);
		$('#contenthere').html(content);
	});
</script>
@endsection