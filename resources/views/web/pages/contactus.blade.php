<section class="section white-bg" id="contact">
	<div id="ancor6"></div>
	<div class="container clearfix">
		<div class="col-lg-12 centered header-wrapper">
			<h2>{{ $setting->contact_title }}</h2>
			<p class="large">{{ $setting->contact_subtitle }}</p>
		</div>
		<div class="col-lg-3 col-md-3 centered contact-wrapper">
			<div class="icons map-pin-1"></div>
			<p class="small">Address</p>
			<p>{{ $setting->contact_address }}</p>
		</div>
		<div class="col-lg-3 col-md-3 centered contact-wrapper">
			<div class="icons fax-machine"></div>
			<p class="small">Phone/Fax</p>
			<p>{!! nl2br(e($setting->contact_phone)) !!}</p>
		</div>
		<div class="col-lg-3 col-md-3 centered contact-wrapper">
			<div class="icons email-2"></div>
			<p class="small">Email</p>
			<p>{!! nl2br(e($setting->contact_email)) !!}</p>
		</div>
		<div class="col-lg-3 col-md-3 centered contact-wrapper">
			<div class="icons network"></div>
			<p class="small">Connect with Us</p>
			<ul class="social-list clearfix">
				<li> <a target="_blank" href="https://www.facebook.com/zycorflyservices/"><i class="fa fa-facebook-square"></i></a> </li>
			</ul>
		</div>
	</div>
</section>