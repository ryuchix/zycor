@include('web.partials.header')
<!-- start header -->
<header class="clearfix">
  <div id="logo"> <a style="font-family: Zycor; font-size: 48px;" href="{{ url('/') }}">Zycor Fly</a> </div>
  <div class="tagline"><span>{!! nl2br(e($setting->site_subheading)) !!}</span></div>
  <div id="nav-button"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
  <nav>
    <ul id="nav">
      <li class="active"><a href="{{ url('/') }}">Home</a> </li>
      <li><a href="{{ url('/#packages') }}">Tour Packages</a> </li>
      <li><a href="{{ url('/#visa') }}">Visa Assistance</a> </li>
      <li><a href="{{ url('/blog/about') }}">About</a></li>
      <li><a href="{{ url('blog') }}">Blog</a></li>
      <li><a href="#contact">Contact</a> </li>
    </ul>
  </nav>
</header>
<!-- end header --> 

<section class="section">
  <div class="container clearfix centered extra-padding-top">
    <div class="col-lg-12">
      <div class="margin-wrapper"><img src="{{ asset('images/blogs/'.$blog->image) }}" alt="" style="" /></div>
    </div>
    <div class="col-lg-8 centered aligncenter padding-30">
      <p><span class="small">{{ \Carbon\Carbon::parse($blog->created_at)->format('F d, Y') }}</span></p>
      <h3>{{ $blog->title }}</h3>
    </div>
  </div>
  <div class="container clearfix padding-60">
    <div class="col-lg-3 col-md-3 centered author-info">
      <div class="avatar"><img alt="" src="{{ url('images/user/'.$blog->user['image']) }}" /></div>
      <h4>{{ $blog->user['name'] }}</h4>
      <p><span class="small">{{ $blog->user['position'] }}</span></p>
      <div class="break"></div>
    </div>
    <article class="col-lg-6 col-md-6">
		{!! nl2br($blog->content) !!}
    </article>
    <div class="col-lg-3 col-md-3 centered padding-10">
      <p class="small">Share:</p>
      <ul class="social-list clearfix">
        <li> <a href="https://plus.google.com/share?url={{ url('blog/'.$blog->slug ) }}"><i class="fa fa-google"></i></a> </li>
        <li> <a target="_blank" href="http://twitter.com/share?text={{ $blog->title }}&url={{ url('blog/'.$blog->slug ) }}&hashtags=zycorfly,cheaptravel,affordabletourpackages,zycorflydavao"><i class="fa fa-twitter"></i></a> </li>
        <li> <a target="_blank" href="http://www.facebook.com/sharer.php?u={{ url('blog/'.$blog->slug ) }}"><i class="fa fa-facebook"></i></a> </li>
        <li> <a target="_blank" href="https://www.instagram.com/?url={{ url('blog/'.$blog->slug ) }}"><i class="fa fa-instagram"></i></a> </li>
        <li> <a href="http://www.digg.com/submit?url={{ url('blog/'.$blog->slug ) }}"><i class="fa fa-digg"></i></a> </li>
      </ul>
      <br>
      <p class="small">Other articles:</p>
      <ul class="clearfix">
        @foreach($blogs as $blog)
        <li>
          <a href="{{ url('blog/'.$blog->slug) }}" alt="{{ $blog->title }}">{{ $blog->title }}</a>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</section>

@include('web.pages.testimonial')
@include('web.pages.contactus')
@include('web.partials.foot')
@include('web.partials.script')