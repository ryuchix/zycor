@include('web.partials.header')

@include('web.pages.hero')

<!-- start header -->
<header class="clearfix">
  <div id="logo"> <a style="font-family: Zycor; font-size: 48px;" href="{{ url('/') }}">Zycor Fly</a> </div>
  <div class="tagline"><span>{!! nl2br(e($setting->site_subheading)) !!}</span></div>
  <div id="nav-button"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
  <nav>
    <ul id="nav">
      <li class="active"><a href="{{ url('/') }}">Home</a> </li>
      <li><a href="{{ url('/#packages') }}">Tour Packages</a> </li>
      <li><a href="{{ url('/#visa') }}">Visa Assistance</a> </li>
      <li><a href="{{ url('/blog/about') }}">About</a></li>
      <li><a href="{{ url('blog') }}">Blog</a></li>
      <li><a href="{{ url('/#contact') }}">Contact</a> </li>
    </ul>
  </nav>
</header>
<!-- end header --> 
<section class="section">
	<div class="container clearfix extra-padding-top">
		<div class="col-md-8 col-sm-6">
			<div class="row">
				<div id="container" class="clearfix">
					<div class="element home  col-md-12">
						<div class="margin-wrapper"> 
							<div class="col-lg-12 aligncenter">
								<div class="flexslider testimonials" style="margin-left: -15px;">
									<ul class="slides">
										@foreach($images as $image)
										<li>
											<img alt="" src="{{ asset('images/packages/'.$image->image) }}" />
										</li>
										@endforeach
									</div>
								</div>
								{!! nl2br($package->content) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 widgets">
				<form id="contact-form" method="post" action="contact.php" role="form">

					<div class="controls">
						<div class="row">
							<div class="col-md-6" style="padding-bottom: 10px;
							text-align: center;
							background-color: #9C27B0;
							width: 100%;">
							<div class="badge badge-success" style="position: absolute;
							top: -12px;
							right: 20px;background-color: #777973;">{{ $package->highlight_name }}</div>
							@if($package->sale == 1)
							<span style="opacity: 0.6; color: #fff;   text-decoration: line-through; display: inline-block;">₱ {{ $package->original_price }}</span>
							@endif
							<h3 style="color: #fff; display: inline-block;">₱ {{ $package->price }}</h3>

						</div>
					</div>
					<div class="row" style="padding-top: 30px; border-left: 1px solid #f1f1f1;
					border-right: 1px solid #f1f1f1;">
					<div class="col-md-6">
						<div class="form-group">
							<input id="form_name" type="text" name="name" class="form-control search-widget" placeholder="Name" required="required" data-error="Firstname is required.">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input id="form_lastname" type="email" name="surname" class="form-control search-widget" placeholder="Email address" required="required" data-error="Lastname is required.">
						</div>
					</div>
				</div>
				<div class="row" style="border-left: 1px solid #f1f1f1;
				border-right: 1px solid #f1f1f1;">
				<div class="col-md-6">
					<div class="form-group">
						<input id="form_email" type="email" name="email" class="form-control search-widget" placeholder="Contact number" required="required" data-error="Valid email is required.">
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input id="form_email" type="email" name="email" class="form-control search-widget" placeholder="Number of guest" required="required" data-error="Valid email is required.">
						<div class="help-block with-errors"></div>
					</div>
				</div>
			</div>
			<div class="row" style="border-left: 1px solid #f1f1f1;
			border-right: 1px solid #f1f1f1; border-bottom: 1px solid #f1f1f1;">
			<div class="col-md-12" style="margin-bottom: -30px;">
				<div class="form-group">
					<textarea id="form_message" name="message" class="form-control search-widget" placeholder="Other info" rows="4" required="required" data-error="Please, leave us a message."></textarea>
				</div>
			</div>
			<div class="col-md-12" style="text-align: right;">
				<a href="https://m.me/izyqin" target="_blank"><img src="{{ asset('assets/img/messenger.png') }}" alt="" width="50px" style="width: 50px; position: absolute;
    left: 21px;
    top: 20%;"></a>

				<p style="position: absolute;
    left: 40%;    font-weight: bolder;
    top: 40%;">OR</p>
				<a href="#" class="button"><i class="fa fa-send"></i>Send</a>
			</div>
		</div>
	</div>

</form>
    <div class="col-lg-12 col-md-12 centered padding-10">
      <h3>Follow us on Facebook</h3>
		<div class="fb-share-button" data-href="{{ url('package/'.$package->slug) }}" data-width="200" data-type="button_count"></div> <br><br>	

		<div class="fb-page" data-href="https://www.facebook.com/zycorflyservices/" data-tabs="timeline" data-width="" data-height="" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-width="500"><blockquote cite="https://www.facebook.com/zycorflyservices/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/zycorflyservices/">Zycor Fly Ticketing Services</a></blockquote></div>	

		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>




    </div>




</div>
</div>
</section>







@include('web.pages.testimonial')
@include('web.pages.contactus')
@include('web.partials.foot')
@include('web.partials.script')