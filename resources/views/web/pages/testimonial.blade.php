<section style="background:url({{ asset('images/settings/'.$setting->testimonial_image) }}) no-repeat center; -webkit-background-size: 100% auto; -moz-background-size: 100% auto; -o-background-size: 100% auto; background-size: 100% auto; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;" class="section with-bg parallax">
	<div class="overlay"></div>
	<div class="container clearfix centered">
		<div class="col-lg-12 centered">
			<h2>{!! nl2br(e($setting->testimonial_title)) !!}</h2>
			<p class="large">{!! nl2br(e($setting->testimonial_subtitle)) !!}</p>
		</div>
		<div class="col-lg-6 aligncenter">
			<div class="flexslider testimonials">
				<ul class="slides">
					@foreach($testimonials as $testimonial)
					<li>
						<blockquote class="clearfix centered">
							<img src="{{ asset('images/testimonials/'.$testimonial->image) }}" alt="{{ $testimonial->name }}, {{ $testimonial->location }}" class="testimonial-img">
							<p>{{ $testimonial->message }}</p>
							<p class="small">{{ $testimonial->name }}, {{ $testimonial->location }}</p>
						</blockquote>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</section>

