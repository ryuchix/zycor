<section class="section white-bg" id="visa">
	<div id="ancor2"></div>
	<!-- start ajax -->
	<div id="ajax-anchor"></div>
	<div class="ajax-content" id="ajax-content"></div>
	<div id="loading">
		<p>loading</p>
	</div>
	<!-- end ajax -->
	<div class="container clearfix">
		<div class="col-lg-12 centered header-wrapper">
			<h2>{!! nl2br(e($setting->visa_title)) !!}</h2>
			<p class="large">{!! nl2br(e($setting->visa_subtitle)) !!}</p>
		</div>
		<div class="col-lg-12 col-md-12 with-bg">
			<div style="text-align: center;">
				<a href="#" class="button" title="">Click here to learn more</a>
			</div>
		</div>
	</div>
</section>