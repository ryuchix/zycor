<!-- start full-page intro -->
<section style="background: url({{ asset('images/settings/'.$setting->hero_image) }}) no-repeat center;" class="bg-image-3 with-bg parallax section" id="section1">
	<div class="overlay">
		<div class="parent">
			<div class="child">
				<h1>{!! nl2br(e($setting->hero_title)) !!}</h1>
				<p class="large">{!! nl2br(e($setting->hero_subtitle)) !!}</p>
			</div>
		</div>
	</div>
	<a href="#packages" data-title="" id="arrow-down" class="aligncenter">Get started</a> 
</section>