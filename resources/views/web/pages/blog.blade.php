@include('web.partials.header')

@include('web.pages.hero')
<!-- start header -->
<header class="clearfix">
  <div id="logo"> <a style="font-family: Zycor; font-size: 48px;" href="{{ url('/') }}">Zycor Fly</a> </div>
  <div class="tagline"><span>{!! nl2br(e($setting->site_subheading)) !!}</span></div>
  <div id="nav-button"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
  <nav>
    <ul id="nav">
      <li class="active"><a href="{{ url('/') }}">Home</a> </li>
      <li><a href="{{ url('/#packages') }}">Tour Packages</a> </li>
      <li><a href="{{ url('/#visa') }}">Visa Assistance</a> </li>
      <li><a href="{{ url('/blog/about') }}">About</a></li>
      <li><a href="{{ url('blog') }}">Blog</a></li>
      <li><a href="#contact">Contact</a> </li>
    </ul>
  </nav>
</header>
<!-- end header --> 

<section class="section">
  <div class="container clearfix extra-padding-top">
    <div class="col-md-9 col-sm-7">
      <div class="row">
        <div id="container" class="clearfix">
          @foreach($blogs as $blog)
          <div class="element home  col-md-6">
            <div class="margin-wrapper"> <img alt="" src="{{ asset('images/blogs/'.$blog->image) }}" />
              <div class="more-info">
                <p><span class="small">{{ $blog->created_at->diffForHumans() }}<span class="padding">&#183;</span>by {{ $blog->user['name'] }}</span></p>
                <h3>{{ $blog->title }}</h3>
                <p>{!! substr($blog->content, 0, 500) !!}</p><br>
                <a href="{{ url('blog/'.$blog->slug) }}" class="button" title="">Read More</a> </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-5 widgets">
        <aside>
          <h5>Recent Posts</h5>
          <ul class="unordered-list clearfix">
            @foreach($blogs as $blog)
            <li><a href="{{ url('blog/'.$blog->slug) }}" title="">{{ $blog->title }}</a></li>
            @endforeach
          </ul>
        </aside>
      </div>
    </div>
    <div class="container clearfix custom-pagination">
      <div class="col-lg-12">
        @if ($blogs->lastPage() > 1)
            <div class="left {{ ($blogs->currentPage() == 1) ? ' inactive' : '' }}">
              <a href="{{ ($blogs->currentPage() == 1) ? 'JavaScript:Void(0);' : ($blogs->currentPage() > 2 ? $blogs->url($blogs->currentPage() -1) : $blogs->url(1)) }}" title="">
                <div class="arrow-left"></div>
                <div class="alignleft">
                  <h5>Previous</h5>
                </div>
              </a>
            </div>
            <div class="right {{ ($blogs->currentPage() == $blogs->lastPage()) ? ' inactive' : '' }}">
              <a href="{{ ($blogs->currentPage() == $blogs->lastPage()) ? 'JavaScript:Void(0);' : $blogs->url($blogs->currentPage()+1)}}" title="">
              <div class="arrow-right"></div>
              <div class="alignright">
                <h5>Older Posts</h5>
              </div>
            </a> 
          </div>
      @endif    





    </div>
  </div>
</section>

@include('web.pages.contactus')
@include('web.partials.foot')
@include('web.partials.script')