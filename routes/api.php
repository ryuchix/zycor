<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
	Route::post('/userlogout', 'UserController@logout');
	Route::post('/adduser', 'UserController@store');
	Route::get('/user/edit/{id}', 'UserController@edit');
	Route::post('/user/update/{id}', 'UserController@update');
	Route::post('/user/change-password/{id}', 'UserController@changePassword');
	Route::post('/user/delete', 'UserController@destroy');
	Route::get('/users', 'UserController@index');
	Route::get('/user', 'UserController@getUser');
	Route::post('/saveprofile', 'UserController@saveProfile');

	Route::get('/packages', 'PackageController@index');
	Route::post('/addpackage', 'PackageController@store');
	Route::post('/package/delete/{id}', 'PackageController@destroy');
	Route::get('/package/edit/{id}', 'PackageController@edit');
	Route::post('/package/update/{id}', 'PackageController@update');
	Route::post('/package/gallery/{id}', 'PackageController@uploadGallery');
	Route::get('/package/galleries/{id}', 'PackageController@getGallery');
	Route::post('/package/gallery/delete/{id}', 'PackageController@deleteGallery');

	Route::get('/testimonials', 'TestimonialController@index');
	Route::post('/addtestimonial', 'TestimonialController@store');
	Route::post('/testimonial/delete/{id}', 'TestimonialController@destroy');
	Route::get('/testimonial/edit/{id}', 'TestimonialController@edit');
	Route::post('/testimonial/update/{id}', 'TestimonialController@update');

	Route::get('/blogs', 'BlogController@index');
	Route::post('/addblog', 'BlogController@store');
	Route::post('/blog/delete/{id}', 'BlogController@destroy');
	Route::get('/blog/edit/{id}', 'BlogController@edit');
	Route::post('/blog/update/{id}', 'BlogController@update');

	Route::get('/settings', 'SiteController@edit');
	Route::post('/settings/update', 'SiteController@update');
});

Route::post('/userlogin', 'UserController@login');

	
	
	
