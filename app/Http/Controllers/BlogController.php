<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

use Validator;

class BlogController extends Controller
{
    public function index()
    {
        return Blog::all();
    }

    public function create()
    {
        //
    }

    public function uploadImage($id) {
        $image = request()->all();
        $rules = array(
            'image' => 'required|mimes:jpeg,jpg,png,gif|max:10000' // max 10000kb
        );
        $validator = Validator::make($image, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 500);
        } else {
            $img = request()->file('image');
            $name = time() . '.' . $img->getClientOriginalName();
            $destinationPath = public_path('images/blogs');
            $upload = $img->move($destinationPath, $name);
            Blog::where('id', $id)->update(['image' => $name]);
        }
    }

    public function store(Request $request)
    {
        $active = $request->active == 'true' ? 1 : 0;

        $blog = new Blog;
        $blog->user_id = auth()->user()->id;
        $blog->title = $request->title == null ? NULL : $request->title;
        $blog->content = $request->content == null ? NULL : $request->content;
        $blog->active = $active;
        $blog->save();

        if ($request->has('image')) {
            $this->uploadImage($blog->id);
        }
        return response()->json(['success' => 'Blog successfully created!'], 200);
    }

    public function show(Blog $blog)
    {
        //
    }

    public function edit($id) {
        return Blog::find($id);
    }

    public function update(Request $request, $id)
    {
        $active = $request->active == 'true' ? 1 : 0;

        $blog = Blog::find($id);
        $blog->title = $request->title == null ? NULL : $request->title;
        $blog->content = $request->content == null ? NULL : $request->content;
        $blog->active = $active;
        $blog->save();

        if ($request->hasFile('image')) {
            $this->uploadImage($id);
        }

        return response()->json(['success' => 'Blog successfully updated!'], 200);
    }

    public function destroy($id)
    {
        $blog = Blog::find($id);
        $blog->delete();

        return response()->json(['success' => 'Blog successfully deleted!'], 200);
    }
}
