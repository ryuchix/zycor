<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SiteSetting;
use Validator;

class SiteController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function all() {
    	return view('welcome');
    }

    public function any() {
    	return view('welcome');
    }

    public function edit() {
        return SiteSetting::find(1);
    }

    public function update(Request $request)
    {
        $site = SiteSetting::find(1);
        $site->site_name = $request->site_name;
        $site->site_subheading = $request->site_subheading;
        $site->hero_title = $request->hero_title;
        $site->hero_subtitle = $request->hero_subtitle;
        $site->tour_title = $request->tour_title;
        $site->tour_subtitle = $request->tour_subtitle;
        $site->visa_title = $request->visa_title;
        $site->visa_subtitle = $request->visa_subtitle;
        $site->testimonial_title = $request->testimonial_title;
        $site->testimonial_subtitle = $request->testimonial_subtitle;
        $site->contact_title = $request->contact_title;
        $site->contact_subtitle = $request->contact_subtitle;
        $site->contact_address = $request->contact_address;
        $site->contact_phone = $request->contact_phone;
        $site->contact_email = $request->contact_email;
        $site->footer_copyright = $request->footer_copyright;
        $site->footer_credit = $request->footer_credit;
        $site->save();

        if ($request->hasFile('hero_image')) {
            $this->uploadHeroImage();
        }

        if ($request->hasFile('testimonial_image')) {
            $this->uploadTestimonialImage();
        }

        return response()->json(['success' => 'Testimonial successfully updated!'], 200);
    }

    public function uploadHeroImage() {
        $img = request()->file('hero_image');
        $name = time() . '.' . $img->getClientOriginalName();
        $destinationPath = public_path('images/settings');
        $upload = $img->move($destinationPath, $name);
        SiteSetting::where('id', 1)->update(['hero_image' => $name]);
    }

    public function uploadTestimonialImage() {
        $img = request()->file('testimonial_image');
        $name = time() . '.' . $img->getClientOriginalName();
        $destinationPath = public_path('images/settings');
        $upload = $img->move($destinationPath, $name);
        SiteSetting::where('id', 1)->update(['testimonial_image' => $name]);
    }
}
