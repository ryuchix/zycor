<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Package;
use App\Testimonial;
use App\SiteSetting;
use App\ImagePackage;
use App\Blog;

class WebController extends Controller
{
    public function web() {

    	$packages = Package::where('active', 1)->get();

    	$testimonials = Testimonial::all();

    	$setting = SiteSetting::find(1);

    	return view('web', compact('packages', 'testimonials', 'setting'));
    }

    public function package($slug) {

    	$packages = Package::where('active', 1)->get();

    	$package = Package::where('slug', $slug)->first();

    	$packagelists = Package::where('active', 1)->get();

    	$testimonials = Testimonial::all();

    	$setting = SiteSetting::find(1);

        $images = ImagePackage::where('package_id', $package->id)->get();

    	return view('web.pages.packagepage', compact('package', 'packagelists', 'setting', 'packages', 'testimonials', 'setting', 'images'));
    }

    public function blog($slug) {

        $blogs = Blog::where('active', 1)->get();

        $blog = Blog::where('slug', $slug)->with('user')->first();

        $bloglists = Blog::where('active', 1)->get();

        $testimonials = Testimonial::all();

        $setting = SiteSetting::find(1);

        $images = ImagePackage::where('package_id', $blog->id)->get();

        return view('web.pages.blogsinglepage', compact('blog', 'bloglists', 'setting', 'blogs', 'testimonials', 'setting', 'images' ));
    }

    public function blogs() {
        $blogs = Blog::where('active', 1)->with('user')->paginate(12);
        $setting = SiteSetting::find(1);

        return view('web.pages.blog', compact('blogs', 'setting' ));
    }
}
