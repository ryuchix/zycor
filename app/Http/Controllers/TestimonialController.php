<?php

namespace App\Http\Controllers;

use App\Testimonial;
use Illuminate\Http\Request;

use Validator;

class TestimonialController extends Controller
{
    public function index()
    {
        return Testimonial::all();
    }

    public function create()
    {
        //
    }

    public function uploadImage($id) {
        $image = request()->all();
        $rules = array(
            'image' => 'required|mimes:jpeg,jpg,png,gif|max:10000' // max 10000kb
        );
        $validator = Validator::make($image, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 500);
        } else {
            $img = request()->file('image');
            $name = time() . '.' . $img->getClientOriginalName();
            $destinationPath = public_path('images/testimonials');
            $upload = $img->move($destinationPath, $name);
            Testimonial::where('id', $id)->update(['image' => $name]);
        }
    }

    public function store(Request $request)
    {
        $testimonial = new Testimonial;
        $testimonial->name = $request->name;
        $testimonial->location = $request->location;
        $testimonial->message = $request->message;
        $testimonial->save();

        if ($request->has('image')) {
            $this->uploadImage($testimonial->id);
        }
        return response()->json(['success' => 'Testimonial successfully created!'], 200);
    }

    public function show(Testimonial $testimonial)
    {
        //
    }

    public function edit($id) {
        return Testimonial::find($id);
    }

    public function update(Request $request, $id)
    {
        $testimonial = Testimonial::find($id);
        $testimonial->name = $request->name;
        $testimonial->location = $request->location;
        $testimonial->message = $request->message;
        $testimonial->save();

        if ($request->hasFile('image')) {
            $this->uploadImage($id);
        }

        return response()->json(['success' => 'Testimonial successfully updated!'], 200);
    }

    public function destroy($id)
    {
        $testimonial = Testimonial::find($id);
        $testimonial->delete();

        return response()->json(['success' => 'Testimonial successfully deleted!'], 200);
    }
}
