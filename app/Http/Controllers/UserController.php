<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;
use Hash;
use Validator;

class UserController extends Controller
{
	public $successStatus = 200;

    public function index() {
        return User::all();
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'email' => 'email|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 500); 
        } else {
            User::create($request->all());
        }

		return response()->json(['success' => 'User successfully created!'], $this->successStatus); 
    }

    public function edit($id) {
        return User::find($id);
    }

    public function update(Request $request, $id) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'email' => 'email|unique:users,email,'. $id
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 500); 
        } else {
            $user = User::find($id);
            User::where('id', $id)->update($request->all());
        }

        return response()->json(['success' => 'User successfully updated!'], $this->successStatus); 
    }

    public function login(Request $request) {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            $user = Auth::user();
            $success['token'] =  $user->createToken($user->id)->accessToken;
            return response()->json(['success' => $success], $this->successStatus); 
        } else { 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    public function changePassword(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 500); 
        } else {
            $user = User::find($id);
            $user->password = Hash::make($request->password);
            $user->save();
        }
        return response()->json(['success' => 'Password successfully updated!'], $this->successStatus);
    }

	public function logout(Request $request) {
	    if (Auth::check()) {
			dd(Auth::user()->token()->revoke());
	        return response()->json(['success' =>'logout_success'], $this->successStatus); 
	    } else {
	        return response()->json(['error' =>'An error has occured!'], 500);
	    }
	}

    public function destroy(Request $request) {
        $user = User::findOrFail($request->id);
        if ($user->delete()) {
            return response()->json(['success' => 'User successfully deleted!'], $this->successStatus);
        } else {
            return response()->json(['error' =>'An error has occured!'], 500);
        }
    }

    public function getUser() {
        return Auth::user();
    }

    public function saveProfile(Request $request) {
        $userid = Auth::user()->id;
        $validator = Validator::make($request->all(), [
            'email' => 'email|unique:users,email,'. $userid
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 500); 
        } else {
            $user = User::find($userid);
            User::where('id', $userid)->update($request->all());
            
            if ($request->has('image')) {
                $this->uploadImage($user->id);
            }
        }
    }

    public function uploadImage($id) {
        $image = request()->all();
        $rules = array(
            'image' => 'required|mimes:jpeg,jpg,png,gif|max:10000' // max 10000kb
        );
        $validator = Validator::make($image, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 500);
        } else {
            $img = request()->file('image');
            $name = time() . '.' . $img->getClientOriginalName();
            $destinationPath = public_path('images/users');
            $upload = $img->move($destinationPath, $name);
            User::where('id', $id)->update(['image' => $name]);
        }
    }
}
