<?php

namespace App\Http\Controllers;

use App\Package;
use Illuminate\Http\Request;

use Validator;
use App\ImagePackage;

class PackageController extends Controller
{

    public function index()
    {
        return Package::all();
    }

    public function create()
    {
        //
    }

    public function uploadImage($id) {
        $image = request()->all();
        $rules = array(
            'image' => 'required|mimes:jpeg,jpg,png,gif|max:10000' // max 10000kb
        );
        $validator = Validator::make($image, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 500);
        } else {
            $img = request()->file('image');
            $name = time() . '.' . $img->getClientOriginalName();
            $destinationPath = public_path('images/packages');
            $upload = $img->move($destinationPath, $name);
            Package::where('id', $id)->update(['image' => $name]);
        }
    }

    public function store(Request $request)
    {
        $highlight = $request->highlight ? 1 : 0;
        $active = $request->active ? 1 : 0;
        $sale = $request->sale ? 1 : 0;

        $package = new Package;
        $package->name = $request->name == null ? NULL : $request->name;
        $package->price = $request->price == null ? NULL : $request->price;
        $package->sale = $sale;
        $package->original_price = $request->original_price == null ? NULL : $request->original_price;
        $package->details = $request->details == null ? NULL : $request->details;
        $package->content = $request->content == null ? NULL : $request->content;
        $package->highlight = $highlight;
        $package->highlight_name = $request->highlight ? $request->highlight_name : null;
        $package->active = $active;
        $package->save();

        if ($request->has('image')) {
            $this->uploadImage($package->id);
        }
        return response()->json(['success' => 'Package successfully created!'], 200);
    }

    public function show(Package $package)
    {
        //
    }

    public function edit($id) {
        return Package::find($id);
    }

    public function update(Request $request, $id)
    {
        $highlight = $request->highlight ? 1 : 0;
        $active = $request->active ? 1 : 0;
        $sale = $request->sale ? 1 : 0;
        
        $package = Package::find($id);
        $package->name = $request->name == null ? NULL : $request->name;
        $package->price = $request->price == null ? NULL : $request->price;
        $package->sale = $sale;
        $package->original_price = $request->original_price == null ? NULL : $request->original_price;
        $package->details = $request->details == null ? NULL : $request->details;
        $package->content = $request->content == null ? NULL : $request->content;
        $package->highlight = $highlight;
        $package->highlight_name = $request->highlight ? $request->highlight_name : null;
        $package->active = $active;
        $package->save();

        if ($request->hasFile('image')) {
            $this->uploadImage($id);
        }  

        return response()->json(['success' => 'Package successfully updated!'], 200);
    }

    public function destroy($id)
    {
        $package = Package::find($id);
        $package->delete();

        return response()->json(['success' => 'Package successfully deleted!'], 200);
    }

    public function uploadGallery($id) {
        $image = request()->all();
        $rules = array(
            'image' => 'mimes:jpeg,jpg,png,gif|max:10000' // max 10000kb
        );
        $validator = Validator::make($image, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 500);
        } else {
            $img = request()->file('gallery');
            $name = time() . '.' . $img->getClientOriginalName();
            $destinationPath = public_path('images/packages');
            $upload = $img->move($destinationPath, $name);

            $imagepackage = new ImagePackage;
            $imagepackage->package_id = $id;
            $imagepackage->image = $name;
            $imagepackage->save();

            return response()->json(['success' => 'Image successfully added!'], 200);
        }
    }

    public function getGallery($id) {
        return ImagePackage::where('package_id', $id)->get();
    }

    public function deleteGallery($id) {
        $imagepackage = ImagePackage::find($id);

        if ($imagepackage->delete()) {
            return response()->json(['success' => 'Gallery successfully deleted!'], 200);
        } else {
            return response()->json(['error' =>'An error has occured!'], 500);
        }
    }
}
