<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\Slug;

class Blog extends Model
{
    use Slug;

    protected static function boot() {
        parent::boot();

        static::creating(function($blog) {
            $blog->slug = $blog->createBlogSlug($blog->title);
        });
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
