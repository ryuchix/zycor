<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\Slug;

class Package extends Model
{
    use Slug;

    protected static function boot() {
        parent::boot();

        static::creating(function($package) {
            $package->slug = $package->createSlug($package->name);
        });
    }

    public function packageImages()
    {
        return $this->hasMany('App\ImagePackage');
    }
}
