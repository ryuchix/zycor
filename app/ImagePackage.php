<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagePackage extends Model
{
    public function package()
    {
        return $this->belongsTo('App\Package');
    }
}
