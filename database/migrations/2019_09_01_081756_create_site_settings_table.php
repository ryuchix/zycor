<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('site_name')->nullable();
            $table->string('hero_title')->nullable();
            $table->text('hero_subtitle')->nullable();
            $table->string('hero_image')->nullable();
            $table->string('tour_title')->nullable();
            $table->text('tour_subtitle')->nullable();
            $table->string('visa_title')->nullable();
            $table->text('visa_subtitle')->nullable();
            $table->string('testimonial_title')->nullable();
            $table->text('testimonial_subtitle')->nullable();
            $table->string('contact_title')->nullable();
            $table->text('contact_subtitle')->nullable();
            $table->text('contact_address')->nullable();
            $table->text('contact_phone')->nullable();
            $table->text('contact_email')->nullable();
            $table->text('footer_copyright')->nullable();
            $table->text('footer_credit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}
